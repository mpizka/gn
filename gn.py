#! /usr/bin/env python3

HELPTEXT = """gn [-n -d] SUBST... FILE...

file renaming tool
apply one or more regex to filenames and rename files
uses python regular expression syntax
https://docs.python.org/3/library/re.html

  -n nono: do not rename just print results
  -d debug: show debugging information
  SUBST: substitution of form s/regex/replace/"""

import os
import os.path as path
import sys
import re

if "-h" in sys.argv or "--help" in sys.argv:
    print(HELPTEXT)
    sys.exit(0)

NONO = True if "-n" in sys.argv else False
DEBUG = True if "-d" in sys.argv else False

REPL = []
FILES = []

for arg in sys.argv[1:]:
    if arg in ["-n", "-d"]:
        continue
    if arg.startswith("s/"):
        _, pattern, replacement, _ = arg.split("/")
        REPL.append((re.compile(pattern), replacement))
        continue
    if not path.isdir(arg):
        FILES.append(arg)

if DEBUG:
    print(f"nono: {NONO}")
    print("-replications-")
    for r in REPL:
        print(f"s/{r[0]}/{r[1]}/")
    print("-filenames-")
    for f in FILES:
        print(f)
    print()

numRenamed = 0
for fn in FILES:
    directory, basename = path.split(fn)
    # apply replacements
    for pattern, replacement in REPL:
        basename = re.sub(pattern, replacement, basename)
    newFn = path.join(directory, basename)
    if NONO or DEBUG:
        print(f"{fn} -> {newFn}")
    if NONO:
        continue
    if path.exists(newFn):
        print(f"won't rename {fn} -> {newFn}: file exists")
        continue
    os.rename(fn, newFn)
    numRenamed += 1

print(f"{numRenamed} files renamed")
