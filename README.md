# gn - goodname

Multi-Pattern python-regex-based file renaming tool for the shell

`gn -h` for usage instructions

# example

`gn -n 's/[^ ßäöüÄÖÜa-zA-Z0-9_\.-]//' 's/ /_/' 's/[_-]+/_/' 's/-.+\.(txt|bak|jpeg)/.\1/' *`

1. remove all characters except ` ßäöüÄÖÜa-zA-Z0-9_\.-`
2. replace spaces with underlines
3. replace streches of underlines and dashes with a single underline
4. replace a trailing `-{somechars}.{extension}` with just the extension
